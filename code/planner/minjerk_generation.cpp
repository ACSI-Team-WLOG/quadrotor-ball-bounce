#include <vector>
#include <math.h>

using namespace std;

// Generate a minimum jerk trajectory from initial and terninal conditions,
//  and return a vector of coefficients (6 coefficients)
vector<double> minjerkGeneration(double x0, double x0d, double x0dd, 
		double xf, double xfd, double xfdd, double T){
	double T2 = T*T; 
	double T3 = T2*T;
	double T4 = T3*T; 
	double T5= T4*T;
	vector<double> a={0.0,0.0,0.0,0.0,0.0,0.0};

	double mat_inv[3][3];
	double c[3];

	int i, j;
	double mat[3][3];
	double determinant = 0;

	// Initial conditions
	a[0] = x0;
	a[1] = x0d;
	a[2] = x0dd/2;

	// Form the coefficient matrix of the system of equations
	mat[0][0] = T3;
	mat[0][1] = T4;
	mat[0][2] = T5;
	mat[1][0] = 3*T2;
	mat[1][1] = 4*T3;
	mat[1][2] = 5*T4;
	mat[2][0] = 6*T;
	mat[2][1] = 12*T2;
	mat[2][2] = 20*T3;

	// b= [T3 T4 T5 ; 3*T2 4*T3 5*T4; 6*T 12*T2 20*T3];

	// Compute the determinant
	for(i = 0; i < 3; i++){
	    determinant = determinant + (mat[0][i] * (mat[1][(i+1)%3] * mat[2][(i+2)%3] - mat[1][(i+2)%3] * mat[2][(i+1)%3]));
	}

	// Compute inverse of coefficient matrix
	for(i = 0; i < 3; i++){
	    for(j = 0; j < 3; j++){
	    	mat_inv[i][j] = ((mat[(j+1)%3][(i+1)%3] * mat[(j+2)%3][(i+2)%3]) - (mat[(j+1)%3][(i+2)%3] * mat[(j+2)%3][(i+1)%3]))/ determinant;
	    }
	}

	// Plug in terminal conditions
	c[0] = xf - a[1] - a[2]*T - a[3]*T2;
	c[1] = xfd - a[2] - 2*a[3]*T;
	c[2] = xfdd - 2*a[3];

	// Compute solution
	for (i=0;i<3;i++){
		a[i+3] = mat_inv[i][0]*c[0] + mat_inv[i][1]*c[1] + mat_inv[i][2]*c[2];
	}
			
	return a;
}
