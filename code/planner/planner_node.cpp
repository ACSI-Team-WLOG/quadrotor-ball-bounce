#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <math.h>

#include <ros/ros.h>
#include <optitrack/UnidentifiedPoint.h>
#include <optitrack/UnidentifiedPointArray.h>

#include "planner.cpp"

using namespace std;

ros::NodeHandle n;

///////// Low pass filter
class LPF{
public:
	LPF(){};
	LPF(double lp_alpha){this->lp_alpha = lp_alpha;};
	double filter(double x_new);
private:
	bool t_init;  // Initial step indicator
	double lp_alpha;
	double x_old;
};

// Low pass filter computation
double LPF::filter(double x_new){
	double x_filtered;

	// Warmstart
	if(t_init){
		x_old = x_new;
		t_init = false;
	}

	// Filter
	x_filtered = (1-lp_alpha)*x_new + lp_alpha*x_old;
	x_old = x_new;

	return x_filtered;
}

///////// 3D State Estimator
class StateEstimator3D{
public:
	StateEstimator3D(double);
	void get_velocity(double, vector<double>*, vector<double>*);
private:
	bool t_init;  // Initial step indicator
	double t_old;
	vector<double> p_old;
	LPF filters[3];
};

// StateEstimator3D constructor
StateEstimator3D::StateEstimator3D(double lp_alpha){
	// Allocate memory for vectors
	p_old.reserve(3);

	// Initialize objects
	for(int i=0; i<3; i++){
		filters[i] = LPF(lp_alpha);
	}
}

// Get velocity from position data
void StateEstimator3D::get_velocity(double t, vector<double>* p_new_ptr, vector<double>* dp_ptr){
	double dt = t - t_old;

	// Warmstart
	if(t_init){
		for(int i=0; i<3; i++){
			p_old[i] = (*p_new_ptr)[i];
		}
		t_init = false;
	}

	// Estimate velocity
	for(int i=0; i<3; i++){
		// Raw velocity estimation from finite difference
		(*dp_ptr)[i] = ((*p_new_ptr)[i] - p_old[i])/dt;

		// Low pass filtering
		(*dp_ptr)[i] = filters[i].filter((*dp_ptr)[i]);
		
		// Update memory
		p_old[i] = (*p_new_ptr)[i];
	}

	// Update time
	t_old = t;
}

void droneCallback(optitrack::UnidentifiedPointArray::ConstPtr& Mocapmsg){
	double t;
	vector<double> drone_p(3);
	vector<double> drone_v(3);

	// Parameters
	static double alpha_drone = 0.7;

	// Estimators as static objects
	static StateEstimator3D drone_estimator(alpha_drone);

	// Unpack data TODO
	t = ;
	drone_p = ;
	
	// Update velocity
	drone_estimator.get_velocity(t, &drone_p, &drone_v);

	// TODO: declare the message and assign the values

	// Publish data
	static ros::Publisher pub_droneState = n.advertise<>("drone_state",10);
	//TODO:assign the value in to the message

	if(ros::ok()){
		pub_droneState.publish()
		ros::spinOnce();
		loop_rate.sleep();
	}
	return;
}

void ballCallback(optitrack::UnidentifiedPointArray::ConstPtr& Mocapmsg){
	double t;
	vector<double> ball_p(3);
	vector<double> ball_v(3);

	// Parameters
	static double alpha_ball = 0.8187;

	// Estimators as static objects
	static StateEstimator3D ball_estimator(alpha_ball);

	// Unpack data TODO
	t = ;
	ball_p = ;
	
	// Update velocity
	ball_estimator.get_velocity(t, &ball_p, &ball_v);

	// TODO: declare the message and assign the values

	// Publish data
	static ros::Publisher pub_ballState = n.advertise<>("ball_state",10);
	//TODO:assign the value in to the message
	if(ros::ok()){
		pub_ballState.publish()
		ros::spinOnce();
		loop_rate.sleep();
	}
	return;

}

int main(){
	// Initialize ROS stuff
	ros::init(argc, argv, "state_estimator");

	ros::Subscriber subBallState = n.subscribe("/optitrack/upoints",10,ballCallback);
	ros::Subscriber subDroneState = n.subscribe("/optitrack/upoints",10,droneCallback);

	ros::Rate loop_rate(100);

	// Let it spin
	ros::spin();

	return 0;
}
