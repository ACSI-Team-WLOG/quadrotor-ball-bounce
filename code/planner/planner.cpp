#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <math.h>

#include <ros/ros.h>
#include <optitrack/UnidentifiedPoint.h>
#include <optitrack/UnidentifiedPointArray.h>
#include <optitrack/RigidBody.h>
#include <optitrack/RigidBodyArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>
#include <std_msgs/Float64MultiArray.h>

#include "ball_predictor.cpp"
#include "minjerk_generation.cpp"

using namespace std;
ros::Publisher* pub_ptr;

class Planner{
public:
	// Trajectory Data
	double T;
	double t0;
	vector<double> x_coeffs(6);
	vector<double> y_coeffs(6);
	vector<double> z_coeffs(6);
	
	// Planner
	Planner(double);
	void updateTrajectory(double, vector<double>, vector<double>, vector<double>, vector<double>);

private:
	// Parameters
	double y_target = 0.25;
	double lp_alpha = 0.8187;
	
	// Ball predictor
	BallPredictor ball_predictor;
};

// Constructor
Planner::Planner(){
	// Initialize predictor
	ball_predictor = BallPredictor(y_target, lp_alpha);

	// Allocate memory for vectors
	x_coeffs.reserve(6);
	y_coeffs.reserve(6);
	z_coeffs.reserve(6);
}

// Main planning algorithm
planner_data_t Planner::updateTrajectory(double t0, vector<double> drone_x, vector<double> drone_v,
		vector<double> ball_x, vector<double> ball_v){
	double x0[3], x0d[3], x0dd[3], xf[3], xfd[3], xfd[3];
	ball_state_t curr_ball_state, target_ball_state;

	// Estimate current state
	curr_ball_state.x = ball_x;
	curr_ball_state.dx = ball_v;	// velocity estimation from external estimator
	curr_ball_state.t = 0;
	
	// Estimate target ball state
	target_ball_state = ball_predictor.predict(curr_ball_state);

	// Update time
	this->t0 = t0;
	T = target_ball_state.t;
	if(T < 0){
		return;
	}

	// Get desired endpoint states
	for (int i=0; i<3; i++){
		x0[i] = drone_x[i];
		x0d[i] = curr_ball_state.dx[i];
		x0dd[i] = 0; // TODO: Get current acceleration
		xf[i] = target_ball_state.x;
		xf[i] = -target_ball_state.dx;
		xfdd[i] = 0;
	}
	
	// Generate trajectory
	x_coeffs = minjerkGeneration(x0[0], x0d[0], x0dd[0], xf[0], xfd[0], xfd[0]);
	y_coeffs = minjerkGeneration(x0[1], x0d[1], x0dd[1], xf[1], xfd[1], xfd[1]);
	z_coeffs = minjerkGeneration(x0[2], x0d[2], x0dd[2], xf[2], xfd[2], xfd[2]);
}

void plannerCallback(const optitrack::UnidentifiedPointArrayConstPtr& Mocapmsg){
	double t0;
	vector<double> drone_x;
	vector<double> drone_v;
	vector<double> ball_x;
	vector<double> ball_v;

	// Static planner object
	static Planner planner_obj;

	// Unpack data
	const optitrack::RigidBody &myMsgD = 
	const optitrack::UnidentifiedPoint &myMsgB = 
	t0 = ros::Time::now().toSec(); //TODO: Get time from estimator
	drone_x = ;
	drone_v = ;
	ball_x = ;
	ball_v = ;
	
	// Plan trajectory
	planner_obj.updateTrajectory(t0, drone_x, drone_v, ball_x, ball_v);

	// Publish data
	planner_obj.x_coeffs
	planner_obj.y_coeffs
	planner_obj.z_coeffs
	planner_obj.t0
	planner_obj.T
	
	pub_ptr->publish();

	return;
}

int main(int argc, char **argv){
	// Initialize ROS stuff
	ros::init(argc, argv, "state_estimator");
	ros::NodeHandle n;

	// Initialize subscribers & publishers
	ros::Subscriber subBallState = n.subscribe("ball_state",10,ballCallback);
	ros::Subscriber subDroneState = n.subscribe("drone_state",10,droneCallback);
	ros::Publisher pubTrajData = n->advertise<>("trajectory_data",10);
	pub_ptr = &pubTrajData;

	ros::Rate loop_rate(100);

	// Let it spin
	ros::spin();

	return 0;
}
