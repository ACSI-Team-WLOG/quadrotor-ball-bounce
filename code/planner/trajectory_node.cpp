#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <cctype>
#include <string>
#include <math.h>

#include <ros/ros.h>
#include <ros/console.h>

#include <optitrack/UnidentifiedPointArray.h>
#include <optitrack/UnidentifiedPoint.h>

using namespace std;
ifstream infile("throw1.txt");


//////////////////////////////Debugging Functions////////////////////////////////////////
//Add a space at the front and back
inline void pushSpace(string &input){
	input.insert(0," ");
	if(!isspace(input.back())){input.insert(input.size()," ");}
	return;
}

//Parse the input string and add the numbers in input string in the vector num
inline void parseAndStore(std::vector<double> &num, string &input){
	pushSpace(input);
	num.clear();
	string temp;
	int count = 0, spacePos = 0;
	for (int i = 0;i<input.length();i++){
		if(isspace(input[i])){
			if(i==0){continue;}
			temp.append(input,spacePos+1,i-(spacePos+1));
 			num.push_back(stod(temp));
			temp.clear();
			spacePos = i;
		}
	}
	input.clear();
	num.erase(num.begin());
	return;
}

//Debug print function
inline void printVector(std::vector<double> &x){
	printf("The vector is ");
	for(int i =0;i<x.size();i++){
		printf("%f ",x[i]);
	}
	printf("\n");
}
///////////////////////////////////////////////////////////////////////////////////

class trajactory_coeff_cal {
	public:

		//current drone position (Assume the velocity and acceleration are zero)
		std::vector<double> drone_pos;

		//Ball prediciton
		std::vector<double> x_new;
		std::vector<double> x_pred = {0.0,0.0,0.0};
		std::vector<double> x_pred_filter= {0.0,0.0,0.0};

		std::vector<double> x_old= {0.0,0.0,0.0};
		std::vector<double> x_pred_old= {0.0,0.0,0.0};
		std::vector<double> dx= {0.0,0.0,0.0};
		std::vector<double> dx_old= {0.0,0.0,0.0};

		std::vector<double> coeffx;
		std::vector<double> coeffy;
		std::vector<double> coeffz;

		double y_target;// = 0.244943;
		// string input;
		double dT;// = 0.005;
		double t_final;// = 0;

	public:
		//Constructor
		void initializer(double y_target, double dT){
			this->y_target = y_target;
			this->dT = dT;
		}

		//Ball physics calculator and change the ball prediction vector 
		void ball_predict(){
			// printf("Start function ball_predict.\n");
			double g = 9.80665;
			double A = -0.5*g; 
			double B = dx[1]; 
			double C = x_new[1]-y_target;
			t_final = 1;
			// std::vector<double> x_pred(3);
			if (B*B-4*A*C>0){
				t_final = (-B-pow(B*B-4*A*C,0.5))/(2*A);
			}

			x_pred[0] = dx[0]*t_final + x_new[0];
			x_pred[1] = y_target;
			x_pred[2] = dx[2]*t_final + x_new[2];
			return;
		}

		//update the ball prediction 
		void ball_predict_process(){

			printf("Start ball_predict_process.\n");

			// pure measurement
			for (int i=0;i<dx.size();i++){
				dx[i] = (x_new[i] - x_old[i])/dT;
			}

			// printf("Low pass filtering.\n");
			// low pass filtering
			double lp_alpha = 0.8187;
			for (int i=0;i<dx.size();i++){
				dx[i] = lp_alpha*dx_old[i] + (1-lp_alpha)*dx[i];
				dx_old[i] = dx[i];
			}

			// printf("Start prediction.\n");
			// initialize x_pred_old first
			ball_predict();

			for(int i = 0;i<x_pred.size();i++){
				x_pred_filter[i] = lp_alpha*x_pred_old[i] + (1-lp_alpha)*x_pred[i];
				x_pred_old[i] = x_pred_filter[i];
			}
			x_old = x_new;

			return;
		}

		//Calculate minimum jerk trajectory and return a vector of coefficients
		std::vector<double> minjerk_coeff(double x0, double x0d, double x0dd, double xf, double xfd, double xfdd){

			double T = t_final;
			double T2 = T*T; 
			double T3 = T2*T;
			double T4 = T3*T; 
			double T5= T4*T;
			std::vector<double> a={0.0,0.0,0.0,0.0,0.0,0.0};

			a[0] = x0;
			a[1] = x0d;
			a[2] = x0dd/2;

			int i, j;
			double mat[3][3];
			double determinant = 0;

			mat[0][0] = T3;
			mat[0][1] = T4;
			mat[0][2] = T5;
			mat[1][0] = 3*T2;
			mat[1][1] = 4*T3;
			mat[1][2] = 5*T4;
			mat[2][0] = 6*T;
			mat[2][1] = 12*T2;
			mat[2][2] = 20*T3;

			// b= [T3 T4 T5 ; 3*T2 4*T3 5*T4; 6*T 12*T2 20*T3];

			//finding determinant
			for(i = 0; i < 3; i++){
			    determinant = determinant + (mat[0][i] * (mat[1][(i+1)%3] * mat[2][(i+2)%3] - mat[1][(i+2)%3] * mat[2][(i+1)%3]));
			}
			// cout<<"\n\ndeterminant: "<<determinant;
			double mat_inv[3][3];

			// cout<<"\n\nInverse of matrix is: \n";
			for(i = 0; i < 3; i++){
			    for(j = 0; j < 3; j++){
			    	mat_inv[i][j] = ((mat[(j+1)%3][(i+1)%3] * mat[(j+2)%3][(i+2)%3]) - (mat[(j+1)%3][(i+2)%3] * mat[(j+2)%3][(i+1)%3]))/ determinant;
			    }
			}

			double c[3];
			c[0] =xf - a[1] - a[2]*T - a[3]*T2;
			c[1] =  xfd - a[2] - 2*a[3]*T;
			c[2] =  xfdd - 2*a[3];


			for (i=0;i<3;i++){
				a[i+3] = mat_inv[i][0]*c[0] + mat_inv[i][1]*c[1] + mat_inv[i][2]*c[2];
			}
			
			return a;
		}

		void generate_coeffs(){

			printf("Start generate_coeffs.\n");

			coeffx.clear();
			coeffy.clear();
			coeffz.clear();

			printVector(drone_pos);
			printVector(x_pred_filter);
			printVector(dx);

			printf("Running Minjerk x\n");
			coeffx = minjerk_coeff(drone_pos[0],0,0,x_pred_filter[0],dx[0],0);
			printf("Running Minjerk y\n");
			coeffy = minjerk_coeff(drone_pos[1],0,0,x_pred_filter[1],dx[1],0);
			printf("Running Minjerk z\n");
			coeffz = minjerk_coeff(drone_pos[2],0,0,x_pred_filter[2],dx[2],0);
			return;
		}

};



int main(int argc, char **argv){

	trajactory_coeff_cal test;
	int count = 0;
	double y_target = 0.244943;
	double dT = 0.005;
	string input;
	test.initializer(y_target,dT);


	while(getline(infile,input)){
		parseAndStore(test.x_new,input);
		test.drone_pos = {0.0,0.0,0.0,0.0};

		printVector(test.x_new);

		test.ball_predict_process();

		if(count > 0){
			test.generate_coeffs();
			break;
		}
		count++;
	}

	printf("Printing coeffx\n");
	printVector(test.coeffx);
	printf("Printing coeffy\n");
	printVector(test.coeffy);
	printf("Printing coeffz\n");
	printVector(test.coeffz);

	

	return 0;
}

//Back up script
// printf("Start reading the file.\n");
	// while(getline(infile,input)){

	// 	getline(infile,input);
	// 	parseAndStore(x_new,input);

	// 	// printf("Pure measument.\n");
	// 	// pure measurement
	// 	for (int i=0;i<dx.size();i++){
	// 		dx[i] = (x_new[i] - x_old[i])/dT;
	// 	}

	// 	// printf("Low pass filtering.\n");
	// 	// low pass filtering
	// 	double lp_alpha = 0.8187;
	// 	for (int i=0;i<dx.size();i++){
	// 		dx[i] = lp_alpha*dx_old[i] + (1-lp_alpha)*dx[i];
	// 		dx_old[i] = dx[i];
	// 	}

	// 	// printf("Start prediction.\n");
	// 	// initialize x_pred_old first
	// 	std::vector<double> x_prediction = ball_predict(x_new,dx,y_target,t_final);


	// 	for(int i = 0;i<x_prediction.size();i++){
	// 		x_pred_filter[i] = lp_alpha*x_pred_old[i] + (1-lp_alpha)*x_prediction[i];
	// 		x_pred_old[i] = x_pred_filter[i];
	// 	}
	// 	x_old = x_new;

		// printVector(x_pred_filter);
	// 	printf("Time:%f\n",t_final);
	// }

