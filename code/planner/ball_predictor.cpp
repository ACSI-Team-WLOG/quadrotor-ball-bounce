#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <cctype>
#include <string>
#include <math.h>

using namespace std;

typedef struct ball_data{
	vector<double> x(3);
	vector<double> dx(3);
	double t;
} ball_data_t;

class BallPredictor {
public:
	BallPredictor(){};
	BallPredictor(double, double, double);
	ball_data_t predict(ball_data_t);
	vector<double> velocity_estimation(vector<double>);

private:
	double lp_alpha;  // Low pass filter param
	double z_target;  // Target height to hit the ball
	double g = 9.80665;

	// memory
	bool t_init;
	vector<double> x_old;
	vector<double> dx_old;
};

// Constructor
BallPredictor::BallPredictor(double z_target, double lp_alpha){
	// Load parameters
	this->z_target = z_target;
	this->lp_alpha = lp_alpha;

	// Reset initial state indicator
	t_init = true;

	// Allocate memory for vectors
	x_old.reserve(3);
	dx_old.reserve(3);
}

// Current state of ball to target state of ball (position & time at y_target)
ball_data_t BallPredictor::predict(ball_data_t state){
	double A = -0.5*g;
	double B = dx[2];
	double C = x[2] - z_target;
	double det;

	ball_state_t state_pred;

	// Time of impact
	det = B*B - 4*A*C;
	if (det > 0){
		state.t = (-B-sqrt(det))/(2*A);
	}else{
		cout << "WARNING: INVALID BALL TRAJECTORY" << endl;
		state.t = -1;
		return state;
	}

	// Impact position
	state_pred.x[0] = state.dx[0]*state.t + state.x[0];
	state_pred.x[1] = state.dx[1]*state.t + state.x[1];
	state_pred.x[2] = z_target;

	// Impact velocity
	state_pred.dx[0] = state.dx[0];
	state_pred.dx[1] = state.dx[1];
	state_pred.dx[2] = -sqrt( state.dx[2]*state.dx[2] + 2*g*(state.x[2] - z_target));

	return state_pred;
}
