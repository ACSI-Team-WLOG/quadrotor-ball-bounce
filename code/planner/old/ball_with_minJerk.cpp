#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <fstream>
#include <cctype>
#include <string>
#include <math.h>

// #include <ros/ros.h>
// #include <ros/console.h>

// #include <geometry_msgs/Pose.h>
// #include <>

using namespace std;
ifstream infile("throw1.txt");

//Add a space at the front and back
inline void pushSpace(string &input){
	input.insert(0," ");
	if(!isspace(input.back())){input.insert(input.size()," ");}
	return;
}

//Parse the input string and add the numbers in input string in the vector num
inline void parseAndStore(std::vector<double> &num, string &input){
	pushSpace(input);
	num.clear();
	string temp;
	int count = 0, spacePos = 0;
	for (int i = 0;i<input.length();i++){
		if(isspace(input[i])){
			if(i==0){continue;}
			temp.append(input,spacePos+1,i-(spacePos+1));
 			num.push_back(stod(temp));
			temp.clear();
			spacePos = i;
		}
	}
	input.clear();
	num.erase(num.begin());
	return;
}



// double y_target = 0.8;
// double dy_target = 3;

// double tf = 0.8;
// double x_pred = 1;
// double z_pred = -2;

// double drone_pos[3];
// double drone_vel[3];
// double drone_acc[3];

// drone_pos[1] = 1;

// std::vector<double> ax[6];
// std::vector<double> ay[6];
// std::vector<double> az[6];
// ax = minjerk_coeff(drone_pos[0],drone_vel[0],drone_acc[0],x_pred,0,0,tf);
// ay = minjerk_coeff(drone_pos[1],drone_vel[1],drone_acc[1],y_target,dy_target,ddy_target,tf);
// az = minjerk_coeff(drone_pos[2],drone_vel[2],drone_acc[2],x_pred,z_pred,0,0,tf);






inline std::vector<double> minjerk_coeff(double x0, double x0d, double x0dd, double xf, double xfd, double xfdd, double tf){

	double T = tf;
	double T2 = T*T; 
	double T3 = T2*T;
	double T4 = T3*T; 
	double T5= T4*T;
	std::vector<double> a(6);

	a[0] = x0;
	a[1] = x0d;
	a[2] = x0dd/2;

	int i, j;
	double mat[3][3];
	double determinant = 0;

	mat[0][0] = T3;
	mat[0][1] = T4;
	mat[0][2] = T5;
	mat[1][0] = 3*T2;
	mat[1][1] = 4*T3;
	mat[1][2] = 5*T4;
	mat[2][0] = 6*T;
	mat[2][1] = 12*T2;
	mat[2][2] = 20*T3;

	// b= [T3 T4 T5 ; 3*T2 4*T3 5*T4; 6*T 12*T2 20*T3];
	//finding determinant
	for(i = 0; i < 3; i++){
	    determinant = determinant + (mat[0][i] * (mat[1][(i+1)%3] * mat[2][(i+2)%3] - mat[1][(i+2)%3] * mat[2][(i+1)%3]));
	}
	// cout<<"\n\ndeterminant: "<<determinant;
	double mat_inv[3][3];

	// cout<<"\n\nInverse of matrix is: \n";
	for(i = 0; i < 3; i++){
	    for(j = 0; j < 3; j++){
	    	mat_inv[i][j] = ((mat[(j+1)%3][(i+1)%3] * mat[(j+2)%3][(i+2)%3]) - (mat[(j+1)%3][(i+2)%3] * mat[(j+2)%3][(i+1)%3]))/ determinant;
	    }
	}

	double c[3];
	c[0] =xf - a[1] - a[2]*T - a[3]*T2;
	c[1] =  xfd - a[2] - 2*a[3]*T;
	c[2] =  xfdd - 2*a[3];

	for (i=0;i<3;i++){
		a[i+3] = mat_inv[i][0]*c[0] + mat_inv[i][1]*c[1] + mat_inv[i][2]*c[2];
	}

	

	return a;
}

inline double minjerk(double t, std::vector<double> a){

	double T = t;
	double T2 = T*T; 
	double T3 = T2*T;
	double T4 = T3*T; 
	double T5= T4*T;

	double pos = a[0]*T + a[1]*T2 + a[2]*T3 + a[3]*T4 + a[4]*T5;
	return pos;

}



inline std::vector<double> ball_predict(std::vector<double> &x0, std::vector<double> &dx0, double y_target, double &t_f){
	// printf("Start function ball_predict.\n");
	double g = 9.80665;
	double A = -0.5*g; 
	double B = dx0[1]; 
	double C = x0[1]-y_target;
	t_f = 1;
	std::vector<double> x_pred(3);
	if (B*B-4*A*C>0){
		t_f = (-B-pow(B*B-4*A*C,0.5))/(2*A);
	}

	x_pred[0] = dx0[0]*t_f + x0[0];
	x_pred[1] = y_target;
	x_pred[2] = dx0[2]*t_f + x0[2];
	return x_pred;
}

inline void printVector(std::vector<double> &x){
	printf("The vector is ");
	for(int i =0;i<x.size();i++){
		printf("%f ",x[i]);
	}
	printf("\n");
}



int main(int argc, char **argv){

	//ROS section
	//Initialize ROS node
	// ros::init(argc, argv, "trajectory_generator");
	// ros::NodeHandle n;
	// //Initialize publisher
	// ros::Publisher pose_pub = n.advertise<geometry_msgs::Pose>("position_update", 1000);

	// //Initialize msg for publishing
	// geometry_msgs::Pose ball_predict;


	// ros::Rate loop_rate(10);

	// int count = 0;
	// while (ros::ok()){
		
	// 	//Assign value into the publisher msg
		
		

	// 	pose_pub.publish(firstPrimAndID);

	// 	ros::spinOnce();
	// 	loop_rate.sleep();
	// 	++count;
	// }


	////////////

	std::vector<double> x_new;
	std::vector<double> x_old(3);
	std::vector<double> x_pred_filter (3);
	std::vector<double> x_pred_old(3);
	std::vector<double> dx(3);
	std::vector<double> dx_old(3);
	double y_target = 0.244943;
	string input;
	double dT = 0.005;
	double t_final = 0;

	printf("Start------------------------------------------------------\n");

	// printf("Start reading the file.\n");
	// while(getline(infile,input)){

	// 	getline(infile,input);
	// 	parseAndStore(x_new,input);

	// 	// printf("Pure measument.\n");
	// 	// pure measurement
	// 	for (int i=0;i<dx.size();i++){
	// 		dx[i] = (x_new[i] - x_old[i])/dT;
	// 	}

	// 	// printf("Low pass filtering.\n");
	// 	// low pass filtering
	// 	double lp_alpha = 0.8187;
	// 	for (int i=0;i<dx.size();i++){
	// 		dx[i] = lp_alpha*dx_old[i] + (1-lp_alpha)*dx[i];
	// 		dx_old[i] = dx[i];
	// 	}

	// 	// printf("Start prediction.\n");
	// 	// initialize x_pred_old first
	// 	std::vector<double> x_prediction = ball_predict(x_new,dx,y_target,t_final);


	// 	for(int i = 0;i<x_prediction.size();i++){
	// 		x_pred_filter[i] = lp_alpha*x_pred_old[i] + (1-lp_alpha)*x_prediction[i];
	// 		x_pred_old[i] = x_pred_filter[i];
	// 	}
	// 	x_old = x_new;

		// printVector(x_pred_filter);
	// 	printf("Time:%f\n",t_final);
	// }

	// plug in the reference trajectory at time 
	std::vector<double> a_x = minjerk_coeff(0, 0, 0, 1, 0, 0, 1);
	std::vector<double> a_y = minjerk_coeff(0, 0, 0, 1, 0, 0, 1);
	std::vector<double> a_z = minjerk_coeff(0, 0, 0, 1, 0, 3, 1);

	printVector(a_x);
	printVector(a_y);
	printVector(a_z);






	

	return 0;
}




