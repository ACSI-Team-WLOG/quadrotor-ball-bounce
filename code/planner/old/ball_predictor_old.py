#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <cctype>
#include <string>
#include <math.h>

using namespace std;

typedef struct ball_data{
	vector<double> x(3);
	vector<double> dx(3);
	double t;
} ball_data_t;

class BallPredictor {
public:
	BallPredictor(){};
	BallPredictor(double, double, double);
	ball_data_t predict(ball_data_t);
	vector<double> velocity_estimation(vector<double>);

private:
	double lp_alpha;  // Low pass filter param
	double y_target;  // Target height to hit the ball
	double g = 9.80665;

	// memory
	bool t_init;
	vector<double> x_old;
	vector<double> dx_old;
};

// Constructor
BallPredictor::BallPredictor(double dT, double y_target, double lp_alpha){
	// Load parameters
	this->dT = dT;
	this->y_target = y_target;
	this->lp_alpha = lp_alpha;

	// Reset initial state indicator
	t_init = true;

	// Allocate memory for vectors
	x_old.reserve(3);
	dx_old.reserve(3);
}

// Current state of ball to target state of ball (position & time at y_target)
ball_data_t BallPredictor::predict(ball_data_t state){
	double A = -0.5*g;
	double B = dx[1];
	double C = x[1] - y_target;
	double det;

	ball_state_t state_pred;

	// Time of impact
	state.t_final = 1;
	det = B*B - 4*A*C;
	if (det > 0){
		state.t = (-B-sqrt(det))/(2*A);
	}else{
		cout << "WARNING: INVALID BALL TRAJECTORY" << endl;
		state.t = -1;
		return state;
	}

	// Impact position
	state_pred.x[0] = state.dx[0]*state.t + state.x[0];
	state_pred.x[1] = y_target;
	state_pred.x[2] = state.dx[2]*state.t + state.x[2];

	// Impact velocity
	state_pred.dx[0] = state.dx[0];
	state_pred.dx[1] = -sqrt( state.dx[1]*state.dx[1] + 2*g*(state.x[1] - y_target));
	state_pred.dx[2] = state.dx[2];

	return state_pred;
}

// Estimate current velocity with new position data point
ball_data_t BallPredictor::velocity_estimation(vector<double> x_new){
	vector<double> dx(3);

	// Warmstart
	if(t_init){
		for (int i=0; i<3; i++){
			x_old[i] = x_new[i];
			dx_old[i] = 0.0;
		}
		t_init = false;
	}

	// Estimation
	for (int i=0; i<3; i++){
		// Raw velocity estimation from finite difference
		dx[i] = (x_new[i] - x_old[i])/dT;

		// Low pass filtering
		dx[i] = lp_alpha*dx_old[i] + (1-lp_alpha)*dx[i];

		// Update memory
		dx_old[i] = dx[i];
		x_old[i] = x_new[i];
	}

	return dx;
}

