#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h> 
#include <std_msgs/Header>

#include "wlog-acsi/BallStateStamped.h" // Need to change to our package name
#include "wlog-acsi/TrajectoryInfo.h" // Need to change to our package name

// Global variables

// Callback for receiving data from optitrack mocap system
// argument poseMsg is a pointer to the pose data that is received
void mocapRcvCallback(const geometry_msgs::PoseStamped::ConstPtr& poseMsg) {

	// Save it to a global variable, or process it here

}

// The ball estimator update function, called from main
wlog-acsi::BallStateStamped updateBallState(wlog-acsi::BallStateStamped *prevBallState) {

	wlog-acsi::BallStateStamped ballState;
	std_msgs::Header ballStateHeader;


}

// Trajectory update function, called from main
wlog-acsi::TrajectoryInfo updateTrajectory() {

}

int main(int argc, char **argv) {

	ros::init(argc, argv, "trajectoryGenerator"); // initialize ROS interface
	ros::NodeHandle n; // Create a handle to the ROS node object

	ros::Subscriber subMocap = n.subscribe("mocapTopicName",10,mocapRcvCallback); // Fix topic name
	ros::Publisher pubBallState = n.advertise<wlog-acsi::BallStateStamped>("ballState",10); //Fix our package name
	ros::Publisher pubTrajInfo = n.advertise<wlog-acsi::TrajectoryInfo>("trajInfo",10); // Fix our package name

	// Set publish rate in Hz
	ros::Rate loop_rate(100);

	wlog-acsi::BallStateStamped ballState;
	wlog-acsi::TrajectoryInfo trajInfo;

	while (ros::ok()) { // Do this while ROS is alive and you're connected

		ballState = updateBallState(&ballState);
		trajInfo = updateTrajectory();

		pubBallState.publish(ballState); // Publish updated ball state
		pubTrajInfo.publish(trajInfo);

		ros::spinOnce(); // spin to check if there are any mocap msgs received
		loop_rate.sleep(); // wait until loop period is up

	}

	return 0;
}