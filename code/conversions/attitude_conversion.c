#include <stdlib.h>
#include <math.h>

void attitude_conversion(double* roll, double* pitch){
	double qx[4], qy[4], q_net[4];
	double tan_newhalfroll, tan_newhalfpitch;

	// original quaternions
	qx[0] = cos(*roll/2);
	qx[1] = sin(*roll/2);
	qx[2] = 0;
	qx[3] = 0;

	qy[0] = cos(*pitch/2);
	qy[1] = 0;
	qy[2] = sin(*pitch/2);
	qy[3] = 0;

	// net rotation
	q_net[0] = qx[0]*qy[0] - qx[1]*qy[1] - qx[2]*qy[2];// - qx[3]*qy[3];
	q_net[1] = qx[0]*qy[1] + qx[1]*qy[0] - qx[2]*qy[3];// + qx[3]*qy[2];
	q_net[2] = qx[0]*qy[2] + qx[1]*qy[3] + qx[2]*qy[0];// - qx[3]*qy[1];
	q_net[3] = qx[0]*qy[3] - qx[1]*qy[2] + qx[2]*qy[1];// + qx[3]*qy[0];

	// new pitch & roll
	tan_newhalfroll = ( sqrt(2)*q_net[3] ) / ( q_net[1] - q_net[2] );
	tan_newhalfpitch = ( sqrt(2)*q_net[3] ) / ( q_net[1] + q_net[2] );
	*roll = 2*atan(tan_newhalfroll);
	*pitch = 2*atan(tan_newhalfpitch);
}
