import numpy
import math

class LPF:
    def __init__(self, lp_alpha):
        self.lp_alpha = lp_alpha
        self.t_init = True

    def filter(self, x_new):
        # Warmstart
        if self.t_init:
            self.x_old = x_new
            self.t_init = False
        
        # Filter
	x_filtered = (1-lp_alpha)*x_new + lp_alpha*x_old
	x_old = x_new;

        return x_filtered

class StateEstimator3D:
    def __init__(self, lp_alpha):
        # Initialize variables
        self.t_init = True
        self.t_old = 0

        # Initialize objects
        self.filters = []
        for _ in range(3):
            self.filters.append(LPF(lp_alpha))

    def get_velocity(self, t, p_new):
        dt = t - self.t_old

        # Warmstart
        if self.t_init:
            self.p_old = p_new
            self.t_init = False

        # Raw velocity estimation from finite difference
        dp = np.zeros(3)
        dp[i] = (p_new[i] - self.p_old[i]) / dt

        # Low pass filtering
        for i in range(3):
            dp[i] = self.filters[i].filter(dp[i])

        # Update memory
        self.p_old = p_new
        self.t_old = t

        return dp

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

@static_vars(drone_estimator=StateEstimator3D(0.7))
def droneCallback(data):
    # Unpack data TODO
    t = 
    drone_p = 

    # Update velocity
    drone_v = drone_estimator.get_velocity(drone_p)

    # Publish data
    drone_p
    drone_v
    

if __name__ == '__main__':
