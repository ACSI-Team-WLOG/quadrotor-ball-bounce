% --- crazyflie parameters
g = 9.80665;


%motor parameters
m = 0.00025707610778491008; %propellor mass  kg
J = 1/12*m*(0.1^2+0.01^2); %moment of inertia from mass, kgm^2
% J needs check
b = 3.5077E-6; %motor viscous friction constant Nms
% b needs check
Ke = 1/1466; %back emf constant V/rad/sec
Kt = Ke; %torque constant Nm/amp
R = 1.03;     %electrical resistance ohm
thrust_max = 0.1; %N, max thrust for single propellor
K_thrust = 2.2*10^-8; %thrust = K*w^2 ,  kg m/rad^2



% % ball parameters
% ball_radius = 0.03;
% ball_density= 10;
% 
% CFR_HomeDir = pwd;
% addpath(genpath(pwd));
% 
% CFL_libname = 'Contact_Forces_Lib';
% load_system(CFL_libname);
% CFL_ver = get_param(CFL_libname,'Description');
% disp(CFL_ver);
% which(CFL_libname)
% 
% tempNumCFLLibs = which(CFL_libname,'-all');