% Simscape(TM) Multibody(TM) version: 5.0

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(36).translation = [0.0 0.0 0.0];
smiData.RigidTransform(36).angle = 0.0;
smiData.RigidTransform(36).axis = [0.0 0.0 0.0];
smiData.RigidTransform(36).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [29.39796452682598 27.135222713891299 1.6499999999999999];  % mm
smiData.RigidTransform(1).angle = 2.5935642459694712;  % rad
smiData.RigidTransform(1).axis = [0.28108463771482528 0.67859834454584589 0.67859834454584611];
smiData.RigidTransform(1).ID = 'B[pcb-1:-:7mm motor mount-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [2.2450640302674483 -3.7500000000000071 4.5078057300644208];  % mm
smiData.RigidTransform(2).angle = 3.1415926535897927;  % rad
smiData.RigidTransform(2).axis = [-0.92387953251128674 -2.2301319646007052e-16 0.38268343236509006];
smiData.RigidTransform(2).ID = 'F[pcb-1:-:7mm motor mount-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [-27.135222713891391 29.397964526825884 1.6499999999999999];  % mm
smiData.RigidTransform(3).angle = 1.7177715174584063;  % rad
smiData.RigidTransform(3).axis = [-0.35740674433659786 -0.86285620946101294 -0.35740674433659786];
smiData.RigidTransform(3).ID = 'B[pcb-1:-:7mm motor mount-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [2.2450640302674443 -3.7500000000000018 4.5078057300644252];  % mm
smiData.RigidTransform(4).angle = 2.5935642459695134;  % rad
smiData.RigidTransform(4).axis = [0.67859834454585088 -0.67859834454585044 -0.28108463771480274];
smiData.RigidTransform(4).ID = 'F[pcb-1:-:7mm motor mount-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [0 14.449082886721394 0];  % mm
smiData.RigidTransform(5).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(5).axis = [1 0 0];
smiData.RigidTransform(5).ID = 'B[pcb-1:-:7mm motor mount-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [31.643028557093043 -2.1000000000000019 46.092111330677518];  % mm
smiData.RigidTransform(6).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(6).axis = [-1 0 0];
smiData.RigidTransform(6).ID = 'F[pcb-1:-:7mm motor mount-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [27.135222713891185 -29.39796452682608 1.6499999999999999];  % mm
smiData.RigidTransform(7).angle = 2.5935642459694681;  % rad
smiData.RigidTransform(7).axis = [0.67859834454584567 -0.28108463771482706 0.67859834454584556];
smiData.RigidTransform(7).ID = 'B[pcb-1:-:7mm motor mount-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [2.2450640302674429 -3.7499999999999956 4.507805730064427];  % mm
smiData.RigidTransform(8).angle = 2.5935642459694801;  % rad
smiData.RigidTransform(8).axis = [0.678598344545847 -0.67859834454584689 -0.28108463771482062];
smiData.RigidTransform(8).ID = 'F[pcb-1:-:7mm motor mount-5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [0 14.449082886721394 0];  % mm
smiData.RigidTransform(9).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(9).axis = [1 0 0];
smiData.RigidTransform(9).ID = 'B[pcb-1:-:battery bc-bl-01-a-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(10).translation = [11.499999999999975 -1.7763568394002505e-15 6.6770828867213927];  % mm
smiData.RigidTransform(10).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(10).axis = [-1 0 0];
smiData.RigidTransform(10).ID = 'F[pcb-1:-:battery bc-bl-01-a-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(11).translation = [0 0 0];  % mm
smiData.RigidTransform(11).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(11).axis = [1 0 0];
smiData.RigidTransform(11).ID = 'B[pcb-1:-:paddle-plate-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(12).translation = [0 10.284924722940936 0];  % mm
smiData.RigidTransform(12).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(12).axis = [0 0.70710678118654746 -0.70710678118654757];
smiData.RigidTransform(12).ID = 'F[pcb-1:-:paddle-plate-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(13).translation = [0 0 0];  % mm
smiData.RigidTransform(13).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(13).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(13).ID = 'B[pcb-1:-:]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(14).translation = [0 0 0];  % mm
smiData.RigidTransform(14).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(14).axis = [7.8504622934188758e-17 7.8504622934188758e-17 -1];
smiData.RigidTransform(14).ID = 'F[pcb-1:-:]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(15).translation = [0 14.449082886721394 1.6499999999999999];  % mm
smiData.RigidTransform(15).angle = 0;  % rad
smiData.RigidTransform(15).axis = [0 0 0];
smiData.RigidTransform(15).ID = 'B[pcb-1:-:header-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(16).translation = [10.329999999999997 8.8817841970012523e-16 2.9070828867213958];  % mm
smiData.RigidTransform(16).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(16).axis = [0 0.70710678118654757 0.70710678118654746];
smiData.RigidTransform(16).ID = 'F[pcb-1:-:header-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(17).translation = [0 14.449082886721394 1.6499999999999999];  % mm
smiData.RigidTransform(17).angle = 0;  % rad
smiData.RigidTransform(17).axis = [0 0 0];
smiData.RigidTransform(17).ID = 'B[pcb-1:-:header-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(18).translation = [10.33 -4.4408920985006262e-16 23.677082886721394];  % mm
smiData.RigidTransform(18).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(18).axis = [0 0.70710678118654757 0.70710678118654746];
smiData.RigidTransform(18).ID = 'F[pcb-1:-:header-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(19).translation = [0 0 0];  % mm
smiData.RigidTransform(19).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(19).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(19).ID = 'B[paddle-plate-1:-:]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(20).translation = [0 10.284924722940936 0];  % mm
smiData.RigidTransform(20).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(20).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(20).ID = 'F[paddle-plate-1:-:]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(21).translation = [0 -10.000000000000005 0];  % mm
smiData.RigidTransform(21).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(21).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(21).ID = 'B[7mm motor mount-3:-:7x15 motor-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(22).translation = [9.8869082499088052e-15 -2.2500000000000062 -2.0196571991730835e-15];  % mm
smiData.RigidTransform(22).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(22).axis = [-0.57735026918962584 -0.57735026918962573 -0.57735026918962573];
smiData.RigidTransform(22).ID = 'F[7mm motor mount-3:-:7x15 motor-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(23).translation = [0 19.5 0];  % mm
smiData.RigidTransform(23).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(23).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(23).ID = 'B[7x15 motor-1:-:propeller_cw-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(24).translation = [31.819806997955638 12.980763541558336 1.4210854715202004e-14];  % mm
smiData.RigidTransform(24).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(24).axis = [0.57735026918962584 -0.57735026918962573 0.57735026918962573];
smiData.RigidTransform(24).ID = 'F[7x15 motor-1:-:propeller_cw-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(25).translation = [0 -10 0];  % mm
smiData.RigidTransform(25).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(25).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(25).ID = 'B[7mm motor mount-5:-:7x15 motor-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(26).translation = [2.6050967856631384e-15 -2.25 -1.5338422409310078e-15];  % mm
smiData.RigidTransform(26).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(26).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(26).ID = 'F[7mm motor mount-5:-:7x15 motor-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(27).translation = [0 19.5 0];  % mm
smiData.RigidTransform(27).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(27).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(27).ID = 'B[7x15 motor-2:-:propeller_cw-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(28).translation = [31.819806997955631 12.980763541558334 0];  % mm
smiData.RigidTransform(28).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(28).axis = [0.57735026918962562 -0.57735026918962562 0.57735026918962595];
smiData.RigidTransform(28).ID = 'F[7x15 motor-2:-:propeller_cw-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(29).translation = [0 -10 0];  % mm
smiData.RigidTransform(29).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(29).axis = [-0.57735026918962573 -0.57735026918962573 -0.57735026918962573];
smiData.RigidTransform(29).ID = 'B[7mm motor mount-1:-:7x15 motor-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(30).translation = [6.3444341012253113e-15 -2.25 4.0552045867402312e-15];  % mm
smiData.RigidTransform(30).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(30).axis = [-0.57735026918962562 -0.57735026918962551 -0.57735026918962606];
smiData.RigidTransform(30).ID = 'F[7mm motor mount-1:-:7x15 motor-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(31).translation = [0 19.5 0];  % mm
smiData.RigidTransform(31).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(31).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(31).ID = 'B[7x15 motor-3:-:propeller_ccw-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(32).translation = [31.819804692149678 12.980763541558341 2.8421709430404007e-14];  % mm
smiData.RigidTransform(32).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(32).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(32).ID = 'F[7x15 motor-3:-:propeller_ccw-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(33).translation = [0 -10.000000000000002 0];  % mm
smiData.RigidTransform(33).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(33).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(33).ID = 'B[7mm motor mount-4:-:7x15 motor-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(34).translation = [3.3960565129245038e-16 -2.2500000000000018 3.7466770670453189e-17];  % mm
smiData.RigidTransform(34).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(34).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962562];
smiData.RigidTransform(34).ID = 'F[7mm motor mount-4:-:7x15 motor-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(35).translation = [0 19.5 0];  % mm
smiData.RigidTransform(35).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(35).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(35).ID = 'B[7x15 motor-4:-:propeller_ccw-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(36).translation = [31.819804692149813 12.98076354155833 -2.4558133304708463e-13];  % mm
smiData.RigidTransform(36).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(36).axis = [0.57735026918962584 -0.57735026918962573 0.57735026918962573];
smiData.RigidTransform(36).ID = 'F[7x15 motor-4:-:propeller_ccw-2]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(8).mass = 0.0;
smiData.Solid(8).CoM = [0.0 0.0 0.0];
smiData.Solid(8).MoI = [0.0 0.0 0.0];
smiData.Solid(8).PoI = [0.0 0.0 0.0];
smiData.Solid(8).color = [0.0 0.0 0.0];
smiData.Solid(8).opacity = 0.0;
smiData.Solid(8).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 0.00051797405477611966;  % kg
smiData.Solid(1).CoM = [2.6004982123444695 -5.1074672136402013 2.6009981455813147];  % mm
smiData.Solid(1).MoI = [0.025697739007362996 0.037430927074318554 0.025701022771673448];  % kg*mm^2
smiData.Solid(1).PoI = [-0.0049253081221420506 -0.014974003872685307 -0.004925838071146979];  % kg*mm^2
smiData.Solid(1).color = [1 1 1];
smiData.Solid(1).opacity = 0.5;
smiData.Solid(1).ID = '7mm motor mount*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 0.001715543649457656;  % kg
smiData.Solid(2).CoM = [0 7.7866059031740926 0];  % mm
smiData.Solid(2).MoI = [0.044361750044357634 0.010462543758413708 0.044361750044357634];  % kg*mm^2
smiData.Solid(2).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(2).color = [0.66666666666666663 0.69803921568627447 0.76862745098039209];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = '7x15 motor*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 0.00025711286476940191;  % kg
smiData.Solid(3).CoM = [31.819805056892957 11.304429424694247 4.8207773898624071e-08];  % mm
smiData.Solid(3).MoI = [0.039718322341075245 0.040182535737756958 0.00077055356096818216];  % kg*mm^2
smiData.Solid(3).PoI = [0 -0.00035293124665678578 0];  % kg*mm^2
smiData.Solid(3).color = [0.071372549019607837 0.071372549019607837 0.071372549019607837];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'propeller_ccw*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 0.00025707610778491008;  % kg
smiData.Solid(4).CoM = [31.819805514751742 11.304445202672339 1.5150971127038768e-07];  % mm
smiData.Solid(4).MoI = [0.039695397633402095 0.040159266025195747 0.00077038568524205565];  % kg*mm^2
smiData.Solid(4).PoI = [0 0.00035295185355067204 0];  % kg*mm^2
smiData.Solid(4).color = [0.071372549019607837 0.071372549019607837 0.071372549019607837];
smiData.Solid(4).opacity = 1;
smiData.Solid(4).ID = 'propeller_cw*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(5).mass = 0.007066481712552016;  % kg
smiData.Solid(5).CoM = [11.5 3.3500000000000005 -8.5];  % mm
smiData.Solid(5).MoI = [0.19637767221968591 0.48120464780625061 0.33761442056655977];  % kg*mm^2
smiData.Solid(5).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(5).color = [0.89803921568627454 0.91764705882352937 0.92941176470588238];
smiData.Solid(5).opacity = 1;
smiData.Solid(5).ID = 'battery bc-bl-01-a*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(6).mass = 0.00016794305442427825;  % kg
smiData.Solid(6).CoM = [10.307417953263977 0.10990679777559247 -1.157];  % mm
smiData.Solid(6).MoI = [0.0015618048006042964 0.0058995837515916048 0.0073463091359851108];  % kg*mm^2
smiData.Solid(6).PoI = [0 0 4.5134259750021978e-06];  % kg*mm^2
smiData.Solid(6).color = [1 1 1];
smiData.Solid(6).opacity = 1;
smiData.Solid(6).ID = 'header*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(7).mass = 0.0027000000000000001;  % kg
smiData.Solid(7).CoM = [0.25243019535907746 0 0.82499999999999996];  % mm
smiData.Solid(7).MoI = [0.4439787417208545 0.45430915069218103 0.89706276741303548];  % kg*mm^2
smiData.Solid(7).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(7).color = [0.792156862745098 0.81960784313725488 0.93333333333333335];
smiData.Solid(7).opacity = 1;
smiData.Solid(7).ID = 'pcb*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(8).mass = 0.0056310620173293255;  % kg
smiData.Solid(8).CoM = [0 1.5 0];  % mm
smiData.Solid(8).MoI = [0.78689073265969456 1.5899404661412404 0.81149632650753978];  % kg*mm^2
smiData.Solid(8).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(8).color = [1 1 1];
smiData.Solid(8).opacity = 0.30000000000000004;
smiData.Solid(8).ID = 'paddle-plate*:*Default';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the PrismaticJoint structure array by filling in null values.
smiData.PrismaticJoint(1).Pz.Pos = 0.0;
smiData.PrismaticJoint(1).ID = '';

smiData.PrismaticJoint(1).Pz.Pos = 0;  % m
smiData.PrismaticJoint(1).ID = '[pcb-1:-:paddle-plate-1]';


%Initialize the RevoluteJoint structure array by filling in null values.
smiData.RevoluteJoint(8).Rz.Pos = 0.0;
smiData.RevoluteJoint(8).ID = '';

smiData.RevoluteJoint(1).Rz.Pos = -100.53247176676217;  % deg
smiData.RevoluteJoint(1).ID = '[7mm motor mount-3:-:7x15 motor-1]';

smiData.RevoluteJoint(2).Rz.Pos = -56.327364676783674;  % deg
smiData.RevoluteJoint(2).ID = '[7x15 motor-1:-:propeller_cw-2]';

smiData.RevoluteJoint(3).Rz.Pos = 147.97751724265399;  % deg
smiData.RevoluteJoint(3).ID = '[7mm motor mount-5:-:7x15 motor-2]';

smiData.RevoluteJoint(4).Rz.Pos = 174.73034128948254;  % deg
smiData.RevoluteJoint(4).ID = '[7x15 motor-2:-:propeller_cw-1]';

smiData.RevoluteJoint(5).Rz.Pos = 153.66324875419008;  % deg
smiData.RevoluteJoint(5).ID = '[7mm motor mount-1:-:7x15 motor-3]';

smiData.RevoluteJoint(6).Rz.Pos = -72.450082299027741;  % deg
smiData.RevoluteJoint(6).ID = '[7x15 motor-3:-:propeller_ccw-1]';

smiData.RevoluteJoint(7).Rz.Pos = -74.465043004168876;  % deg
smiData.RevoluteJoint(7).ID = '[7mm motor mount-4:-:7x15 motor-4]';

smiData.RevoluteJoint(8).Rz.Pos = -68.243198281411466;  % deg
smiData.RevoluteJoint(8).ID = '[7x15 motor-4:-:propeller_ccw-2]';

