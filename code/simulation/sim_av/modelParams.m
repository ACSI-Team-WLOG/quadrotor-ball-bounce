motorParams.R = 1.03; % [Ohms]
motorParams.Kt = 1466; % [A/Nm]
motorParams.I_noload = 1e-3; % [A] ???
motorParams.V_app_noload = 1.5; % [V] ???
motorParams.L = 1e-5; % [H] ???

ballParams.radius = 40e-3; % [m]
ballParams.mass = 2.7e-3; % [kg]
ballParams.initPose = [0 0 -5]; % [m] (x y z)

quadParams.mass = 0.03097; % [kg]
quadParams.CoM = [0 0 0]; % [m]
quadParams.inertia_moments = [1.112951e-5 1.114361e-5 2.162056e-5]; % [kg*m^2]
quadParams.initPose = [0 0 -1]; % [m] (x y z)

electronicsParams.Battery.V_max = 4.2; % [V]
electronicsParams.Battery.V_min = 3.3; % [V]
electronicsParams.Battery.R_out = 50e-3; % [Ohm]

electronicsParams.PWM.freq = 1e3; % [Hz] ???
electronicsParams.PWM.V_out_max = 3.3; % [V]

electronicsParams.FET.R_ds_on = 44e-3; % [Ohm]
electronicsParams.FET.I_ds_on = 1; % [A]
electronicsParams.FET.V_gs_on = 2.5; % [V]
electronicsParams.FET.V_gs_th = 1; % [V]
electronicsParams.FET.V_diode = 0.6; % [V]

electronicsParams.FlybackDiode.V_f = [320e-3 800e-3]; % [V]
electronicsParams.FlybackDiode.I_f = [1e-3 100e-3]; % [A]
electronicsParams.FlybackDiode.ESR = 1e-2; % [Ohm]
electronicsParams.FlybackDiode.C_j0 = 10; % [pF]





