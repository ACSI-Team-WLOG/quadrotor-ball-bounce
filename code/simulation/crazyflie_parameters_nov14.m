% --- crazyflie parameters
g = 9.80665;

l1 = 15;
l2 = 5; %propellor length in cm

%motor parameters
L = 2.75E-6; %H
m = 0.001; %propellor+motor mass kg
J = 1/12*m*(0.1^2+0.01^2); %moment of inertia from mass, kgm^2
b = 3.5077E-6; %motor viscous friction constant Nms
Ke = 0.000174; %back emf constant V/rad/sec
Kt = Ke; %torque constant Nm/amp
R = 0.1;     %electrical resistance ohm
thrust_max = 0.1; %N, max thrust for single propellor
K_thrust = 0.0000003;