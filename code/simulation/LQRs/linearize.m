function [A, B] = linearize(f, x_l, u_l, x_l0, u_l0)
%LINEARIZE: Linearizes system equations around designated location
n_dof = length(x_l0)/2;
n_inputs = length(u_l0);

% Linearization at operatiing point
v_l = [x_l, u_l];
v_l0 = [x_l0, u_l0];
f_lin_p = subs(f, v_l, v_l0);
f_lin_d = subs(jacobian(f, v_l), v_l, v_l0)*(v_l - v_l0).';
f_lin = f_lin_p + f_lin_d;
%fprintf('Linearized system equations:\n');
%disp(vpa(f_lin,4));

% Organize terms into linear system matrices
A = zeros(2*n_dof);
B = zeros(2*n_dof,n_inputs);
for i = 1:2*n_dof
    for j = 1:2*n_dof
        sub_vec = zeros(1,2*n_dof+n_inputs);
        sub_vec(j) = 1;
        if has(f_lin(i), v_l(j))
            A(i,j) = subs(f_lin(i), v_l, sub_vec);
        end
    end
	for Cr = 1:n_inputs
        sub_vec = zeros(1,2*n_dof+n_inputs);
        sub_vec(2*n_dof + Cr) = 1;
        if has(f_lin(i), v_l(2*n_dof + Cr))
            B(i,Cr) = subs(f_lin(i), v_l, sub_vec);
        end
    end
end

end

