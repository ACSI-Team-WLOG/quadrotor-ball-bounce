function [f, q, q_dot, u] = quad_eqngen(quad_params)
%QUAD_SYSEQNS: Generates system equations of a quadrotor

% System parameters
%   quadrotor
m = quad_params(1);
Jx = quad_params(2);
I = diag([Jx, Jx, 2*Jx]);
L = quad_params(3);

%	physical constants
g = 9.80665;

% Generalized coordinates
n_dof = 6;
n_inputs = 4;
syms t real;
q = sym('q', [n_dof,1]);
q_dot = sym('q_dot', [n_dof,1]);
u = sym('u', [n_inputs,1]);

% Readable symbolic variables
x=q(1); y=q(2); z=q(3); rx=q(4); ry=q(5); rz=q(6);
x_dot=q_dot(1); y_dot=q_dot(2); z_dot=q_dot(3); rx_dot=q_dot(4); ry_dot=q_dot(5); rz_dot=q_dot(6);
ft=u(1); tx=u(2); ty=u(3); tz=u(4);

% -------------- Derive system equations --------------
% Trigonometric functions
c = @(p) cos(p);
s = @(p) sin(p);

% System dynamics  q_dot = f(q, u)
f = [
    x_dot;
    y_dot;
    z_dot;
    rx_dot;
    ry_dot;
    rz_dot;
    -(ft/m)*(s(rx)*s(rz) + c(rx)*c(rz)*s(ry));
    -(ft/m)*(c(rx)*s(rz)*s(ry) - c(rz)*s(rx));
    g - (ft/m)*(c(rx)*c(ry));
    ( (I(2,2) - I(3,3))/I(1,1) )*ry_dot*rz_dot + tx/I(1,1);
    ( (I(3,3) - I(1,1))/I(2,2) )*rx_dot*rz_dot + ty/I(2,2);
    ( (I(1,1) - I(2,2))/I(3,3) )*rx_dot*ry_dot + tz/I(3,3);
];


end

