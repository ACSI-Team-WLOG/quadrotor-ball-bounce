function [v2u, u2v] = vu_conversion( quad_params, motor_params )
%VU_CONVERSION: returns relations between input voltages (v)
%               and forces/torques (u)

% load quad parameters
L = quad_params(3);

% load motor parameters
Ke = motor_params(1);
Kt = motor_params(2);
Rm = motor_params(3);
bm = motor_params(4);        % tau_propeller = bm * w^2
K_thrust = motor_params(5);	% f_propeller = K_thrust * w^2

% create symbolic variables
v = sym('v', [4,1]);
u = sym('u', [4,1]);

% w^2 to f matrix
A = [
    K_thrust K_thrust K_thrust K_thrust;
    -K_thrust*L 0 K_thrust*L 0;
    0 -K_thrust*L 0 K_thrust*L;
    -bm bm -bm bm;
];

% voltage to forces
w = v * Kt / (Rm*bm + Kt*Ke);
%ft = b*(w(1)^2 + w(2)^2 + w(3)^2 + w(4)^2);  % vertical force
%tau = [     % rotational torque
%	b*(w(3)^2 - w(1)^2)*L;
%	b*(w(4)^2 - w(2)^2)*L;	
%	Cr*(w(4)^2 - w(3)^2 + w(2)^2 - w(1)^2);
%];
%u_out = [ft, tau(1), tau(2), tau(3)];

u_out = A*w.^2;
v2u = matlabFunction(u_out);

% forces to voltage
w_sq = A\u;
v_out = (w_sq.^0.5)*(Rm*bm + Kt*Ke)/Kt;

u2v = matlabFunction(v_out);

end

