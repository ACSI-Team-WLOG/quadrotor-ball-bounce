function r = quat2rot(q)

w = q(1); x = q(2); y = q(3); z = q(4);

ysqr = y * y;
	
t0 = +2.0 * (w * x + y * z);
t1 = +1.0 - 2.0 * (x * x + ysqr);
X = atan2(t0, t1);
	
t2 = +2.0 * (w * y - z * x);
if t2 > +1.0
    t2 = +1.0;
end
if t2 < -1.0
    t2 = -1.0;
end
Y = asin(t2);
	
t3 = +2.0 * (w * z + x * y);
t4 = +1.0 - 2.0 * (ysqr + z * z);
Z = atan2(t3, t4);
	
r = [X, Y, Z];
