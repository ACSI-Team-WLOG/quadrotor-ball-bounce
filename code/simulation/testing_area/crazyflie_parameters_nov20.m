% --- crazyflie parameters
g = 9.80665;


l1 = 4.65; %cm  1/2 of length of body
l2 = 4.5;


%motor parameters
m = 0.00020; %propellor mass  kg
J = 1/12*m*(0.1^2+0.01^2); %moment of inertia from mass, kgm^2
% J needs check
b = 3.5077E-10; %motor viscous friction constant Nms
% b needs check

%drone parameters
m_drone = 0.03097;   % kg
Ix_drone = 1.1122951e-5;  % kg*m^2
Iy_drone = 1.1143608e-5;  % kg*m^2
Iz_drone = 2.162056e-5;  % kg*m^2



% Ke = 1/2000; %back emf constant V/rad/sec
% Kt = Ke; %torque constant Nm/amp
% R = 1.03;     %electrical resistance ohm
% thrust_max = 0.1; %N, max thrust for single propellor
% K_thrust = 2.2*10^-8; %thrust = K*w^2 ,  kg m/rad^2



% % ball parameters
% ball_radius = 0.03;
% ball_density= 10;
% 
% CFR_HomeDir = pwd;
% addpath(genpath(pwd));
% 
% CFL_libname = 'Contact_Forces_Lib';
% load_system(CFL_libname);
% CFL_ver = get_param(CFL_libname,'Description');
% disp(CFL_ver);
% which(CFL_libname)
% 
% tempNumCFLLibs = which(CFL_libname,'-all');