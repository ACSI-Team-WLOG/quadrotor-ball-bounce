#include <stdlib.h>
#include <math.h>

double* force2pwm(double f_thrust, double tau[3]){
	double f_motor[4];
	double L = 0.0465;
	double km = 0.005964552;
	double sqrt2 = sqrt(2);

	double* pwm = malloc(4*sizeof(double));

	/*// convert Cartesian forces to individual motor forces (T)
	f_motor[0] = 0.25*f_thrust + 0 + (1/(2*L))*tau[1] + (1/(4*km))*tau[2];
	f_motor[1] = 0.25*f_thrust - (1/(2*L))*tau[0] + 0 - (1/(4*km))*tau[2];
	f_motor[2] = 0.25*f_thrust + 0 - (1/(2*L))*tau[1] + (1/(4*km))*tau[2];
	f_motor[3] = 0.25*f_thrust + (1/(2*L))*tau[0] + 0 - (1/(4*km))*tau[2];
	*/

	// convert Cartesian forces to individual motor forces (X)
	f_motor[0] = 0.25*f_thrust - (1/(2*sqrt2*L))*tau[0] + (1/(2*sqrt2*L))*tau[1] + (1/(4*km))*tau[2];
	f_motor[1] = 0.25*f_thrust - (1/(2*sqrt2*L))*tau[0] - (1/(2*sqrt2*L))*tau[1] - (1/(4*km))*tau[2];
	f_motor[2] = 0.25*f_thrust + (1/(2*sqrt2*L))*tau[0] - (1/(2*sqrt2*L))*tau[1] + (1/(4*km))*tau[2];
	f_motor[3] = 0.25*f_thrust + (1/(2*sqrt2*L))*tau[0] + (1/(2*sqrt2*L))*tau[1] - (1/(4*km))*tau[2];
	
	// map motor forces to pwm signals
	for(int i=0; i<4; i++){
		pwm[i] = 2.347e10*sqrt(8.521e-11*f_motor[i] + 1.02e-12) - 24244.0;
	}

	return pwm;
}
