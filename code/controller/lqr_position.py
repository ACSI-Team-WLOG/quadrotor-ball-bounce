#!/usr/bin/env python
        
import numpy as np

# Off-board LQR controller for position tracking
class PositionLQR:
    def __init__(self, dt, K_lqr_p, g):
        self.dt = dt
        
        # Params
        self.g = g

        # Gain
        self.K = K_lqr_p

        # Initialize memory
        self.p_desired_prev = None
        self.dp_desired_prev = None
        self.p_prev = None

    # Controller step
    def step(self, p_desired, p, r_yaw_desired, r_yaw):
        # Update derivatives
        if self.p_desired_prev is None:
            self.p_desired_prev = p_desired
        dp_desired = (p_desired - self.p_desired_prev)/self.dt

        if self.dp_desired_prev is None:
            self.dp_desired_prev = dp_desired
        ddp_desired = (dp_desired - self.dp_desired_prev)/self.dt

        if self.p_prev is None:
            self.p_prev = p
        dp = (p - self.p_prev)/self.dt

        # Operating thrust
        f_op = np.linalg.norm(ddp_desired - self.g)

        # State error
        e = p_desired - p
        de = dp_desired - dp

        # LQR control
        f = np.dot(self.K, np.concatenate([e, de]))
        f_thrust = f[0] + f_op
        r_ref = f[1:4]

        # Explicit yaw control
        r_ref[2] = r_yaw_desired - r_yaw

        # Update memory
        self.p_desired_prev = p_desired
        self.dp_desired_prev = dp_desired
        self.p_prev = p

        return f_thrust, r_ref
