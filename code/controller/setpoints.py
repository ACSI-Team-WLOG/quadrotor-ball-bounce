#!/usr/bin/env python
        
import numpy as np

# Trajectory generator TODO: Min-jerk
def refTrajectory(t):
    # interpolation function
    interp = lambda p0,p1,t0,t1,t: p0 + (t-t0)/(t1-t0)*(p1-p0)

    # waypoints
    t_ls = [0, 2, 3, 5]
    x_ls = [0, 0, 0, 1]
    y_ls = [0, 0, 0, 0]
    z_ls = [0, -1, -1, -1]

    # stabilize at last point
    t_ls.append(9999)
    x_ls.append(x_ls[-1])
    y_ls.append(y_ls[-1])
    z_ls.append(z_ls[-1])

    # parse
    for i in len(t_ls):
        if t_ls[i] > t:
            curr = i

    t0 = t_ls[curr-1]
    t1 = t_ls[curr]
    p0 = np.array([x_ls[curr-1], y_ls[curr-1], z_ls[curr-1]])
    p1 = np.array([x_ls[curr], y_ls[curr], z_ls[curr]])

    # interpolate
    p = interp(p0, p1, t0, t1, t)

    return p