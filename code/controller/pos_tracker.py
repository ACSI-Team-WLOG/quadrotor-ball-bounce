#!/usr/bin/env python
        
import rospy
import numpy as np
from geometry_msgs.msg import Twist

from lqr_position import PositionLQR
from setpoints import refTrajectory

# Global data (updated by ROS callbacks)
global t, p, r, ball_p, ball_v, traj_coeffs, t0_traj, T_traj

# Controller
#   A controller that switches between modes via a finite state machine
#   states: 0-waiting, 1-tracking, 2-hitting
class CrazyFlieController:
    def __init__(self, dt):
        self.dt = dt

        # Controller parameters
        self.g = np.array([0,0,9.80665])
        self.T_2hit = 0.1   # Trajectory time lower bound
        self.z_wait = 0.3   # Idle height
        self.z_2track = 1   # Ball height threshold
        K_lqr_p = np.array([
            [0,         0,  -0.3156,         0,         0,    -0.1676],
            [0,    0.9978,         1,         0,    0.4511,         0],
            [-0.9958,   0,         0,   -0.4511,         0,         0],
            [0,         0,         0,         0,         0,         0],
            ])

        # Architecture parameters
        self.poly_n = 5  # Trajectory polynomial order
        self.yaw_ref = 0 #TODO: Check if dats true

        # Initialize LQR controller
        self.lqr = PositionLQR(dt, K_lqr_p, self.g)

        # Initialize variables
        self.state = -1
        self.t_hitting = 0

    # Controller step
    def step(self): 
        global p, r, ball_p, ball_v, traj_coeffs, t0_traj, T_traj

        # Update state
        #self.stateUpdate(p, ball_p, ball_v, T_traj)
        self.state = 1 # for waypoint test

        # Control based on current state
        if self.state == 0:  # Waiting: Stabilize at a certain height
            # LQR
            return self.lqr.step(self.p_wait, 0, 0, p, self.yaw_ref, r[2])

        elif self.state == 1:  # Tracking: Track ref position with LQR
            # Get position from trajectory
            #p_ref, v_ref, a_ref = self.poly2pos(t - t0_traj, traj_coeffs)
            p_ref, v_ref, a_ref = refTrajectory(t) # for waypoint test
            # LQR
            return self.lqr.step(p_ref, v_ref, a_ref, p, self.yaw_ref, r[2])

        elif self.state == 2:  # Hitting: Stabilize attitude with high thrust
            # Update time
            self.t_hitting += self.dt

            # Stabilize attitude & Max out thrust
            return 0.4, np.zeros(3)

    # State transitions
    def stateUpdate(self, p, ball_p, ball_v, T_traj):
        if self.state == -1:  # Initialize
            # Transition: To Waiting
            self.p_wait = np.array(p[0], p[1], self.z_wait)

        if self.state == 0:  # Waiting
            # Transition: To Tracking
            if self.goHam(ball_p, ball_v):
                self.state = 1

        elif self.state == 1:  # Tracking
            # Transition: To Waiting
            if T_traj <= 0:
                self.state = 0
                self.p_wait = np.array(p[0], p[1], self.z_wait)

            # Transition: To Hitting
            elif T_traj <= self.T_2hit:
                self.state = 2
                self.t_hitting = 0

        elif self.state == 2:  # Hitting
            # Transition: To Waiting
            if self.t_hitting > 2*self.T_2hit:
                self.state = 0
                self.p_wait = np.array(p[0], p[1], self.z_wait)
    

    # Decide whether ball is ready to be chased
    def goHam(self, ball_p, ball_v):
        g = 9.8
        height_criterion = ( ball_p[2] + ball_v[2]**2 / (2*g) > self.z_2track )
        speed_criterion = ( ball_v[2] > 0 )
        return (height_criterion and speed_criterion)

    # Calculate reference position, velocity, acceleration from reference trajectory
    def poly2pos(self, coeffs, t):
        p_ref = np.zeros(3)
        v_ref = np.zeros(3)
        a_ref = np.zeros(3)
        for d in range(3):
            for i in range(self.poly_n+1):
                p_ref[d] += coeffs[d*(self.poly_n+1) + i] * (t**i)
                if i >= 1:
                    v_ref[d] += coeffs[d*(self.poly_n+1) + i] * i * (t**(i-1))
                if i >= 2:
                    a_ref[d] += coeffs[d*(self.poly_n+1) + i] * i*(i-1) * (t**(i-2))
            # Global coordinate to Tait-Bryan
            if d > 0:
                v_ref[d] = -v_ref[d]
                a_ref[d] = -a_ref[d]

        return p_ref, v_ref, a_ref


# Pack controller outputs into a twist TODO
def packTwist(f_thrust, r_ref):
    twist = Twist()

    twist.linear.z = 256*(1222.5*np.sqrt(0.001636*(1000*f_thrust/9.80665) + 0.019902) - 171.76)
    twist.linear.y = r_ref[0] * (180/np.pi)
    twist.linear.x = r_ref[1] * (180/np.pi)
    twist.angular.z = r_ref[2] * (180/np.pi)

    return twist

def mocap_callback(data):
    # Position
    global p
    p_x = data.bodies[0].pose.position.x   
    p_y = -data.bodies[0].pose.position.y    
    p_z = -data.bodies[0].pose.position.z    
    p = np.array([p_x, p_y, p_z]) # low pf?

    # Orientation
    global r
    w = data.bodies[0].pose.orientation.w   
    x = data.bodies[0].pose.orientation.x    
    y = data.bodies[0].pose.orientation.y
    z = data.bodies[0].pose.orientation.z

    r_x, r_y, r_z = quat2euler(w,x,y,z)

    r = np.array([r_x, r_y, r_z]) # low pf?

def planner_callback(data): #TODO: get the data from the planner
    # Ball data
    global ball_p, ball_v
    #ball_p = 
    #ball_v = 
    ball_p = 0  # for waypoint test
    ball_v = 0  # for waypoint test

    # Min-jerk planner data
    global traj_coeffs, t0_traj, T_traj
    #traj_coeffs =  # array with length 3*6
    #t0_traj = 
    #T_traj = 
    T_traj = 999 # for waypoint test

def quat2euler(w, x, y, z):
    ysqr = y * y
    
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + ysqr)
    X = np.atan2(t0, t1)
    
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    Y = np.asin(t2)
    
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (ysqr + z * z)
    Z = np.atan2(t3, t4)
    
    return X, Y, Z


# Main function
if __name__ == '__main__':
    global t, p, r, ball_p, ball_v, traj_coeffs, t0_traj, T_traj

    # Initialize ROS stuff
    rospy.init_node('crazyflie_demo_const_thrust', anonymous=True)

    # Publish and subscriber
    p = rospy.Publisher('cmd_vel', Twist)
    rospy.Subscriber("/optitrack/rigid_bodies",RigidBodyArray, mocap_callback)
    #TODO: subscribe to planner

    # Timestep (for differentiation)
    #r = rospy.Rate(50)
    #dt = 1.0/r

    # Initialize controller
    dt = 0.01  # Controller timestep
    controller = CrazyFlieController(dt)

    # Initialize global variables
    t = None
    p = None
    r = None
    ball_p = None
    ball_v = None
    traj_coeffs = None
    t0_traj = None
    T_traj = None

    # Main loop
    t_last = rospy.get_time()
    while not rospy.is_shutdown():
        # Constant timestep
        t = rospy.get_time()
        if t < t_last + dt:
            continue

        # For waypoint test
        planner_callback(None)

        # Check data from all sources are received
        for glovar in [p, r, ball_p, ball_v, traj_coeffs, t0_traj, T_traj]:
            if glovar is None:
                continue

        # Controller step
        f_thrust, r_ref = controller.step()

        # Pack controller outputs into a twist
        myTwist = packTwist(f_thrust, r_ref)
        p.publish(myTwist)

        # Update time
        t_last = t
