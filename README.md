# ACSI Team WLOG Final Project -- Quadrotor Ball Bouncing

## Useful Data / Facts
- Crazyflie 2.0
	- Physical Stats:
	    - Max takeoff weight -- ~42g
        - Base weight (with battery) -- ~27g
        - Total weight with optical flow deck (and battery) -- ~31g
        - https://wiki.bitcraze.io/misc:investigations:thrust
	- Electronics
	    - On-board MCU: STM32F405RG  
	        - ARM 32-bit Cortex-M4F
	        - http://www.st.com/en/microcontrollers/stm32f405rg.html

## Troubleshooting
See the `notes/` folder for various tidbits of knowledge that might help troubleshoot issues.

# Resources
- Crazyflie
	- https://www.bitcraze.io/crazyflie-2/
	- https://github.com/bitcraze/crazyflie-clients-python
	- https://github.com/bitcraze/crazyflie-firmware
	- https://github.com/bitcraze/crazyflie-lib-python