# XBox 360 (Wired) Controller Setup / Notes

## Linux Driver(s)
The linux kernel handles most modern usb joysticks through the standard `usbhid` module for the low-level USB enumeration, along with the `joydev` module for joystick-specific interfacing. Both of these come standard on most (if not all?) Debian derivatives.

Both `xpad` and `xboxdrv` support XBox 360 controllers (reportedly both wired and wireless, though I have only tested wired). `xpad` is a kernel module, whereas `xboxdrv` is a userspace program (can be run as a service daemon). The two are mutually exclusive -- use one or the other.

Historically, it seems `xboxdrv` was designed as an alternative to `xpad`, with the intent of having additional functionality as well as the benefits (ease of use, I guess) of running in userspace. Interestingly, I have seen conflicting reports online about how the two "drivers" compare...

So far it seems `xpad` works out of the box with the controller I have and the Crazyflie GUI (the mappings make sense), but only when I execute as superuser... Could be a simple permissions issue, or perhaps more likely a weird thing with my python install (I didn't use a python virtual environment)



For our purposes, we'll just want to keep the gamepad functionality around to allow some operator control during development -- things like switching between manual and auto mode, perhaps trimming certain parameters, etc. Whichever interface is easier to get running should be sufficient for our needs.

TODO: 
- [ ] Get gamepad working in Crazyflie GUI without having to execute as root
- [ ] Figure out if there are significant differences between `xpad` and `xboxdrv` in terms of capability.

- References
	- https://wiki.archlinux.org/index.php/Gamepad

