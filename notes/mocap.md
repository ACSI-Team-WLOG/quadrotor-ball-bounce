# Wean 2310 Motion Capture Setup / Notes

## Turning Things On
- The MoCap system is a 12 (is that right?) OptiTrack (TODO: what cameras?) setup
- The PC in the middle of the desk on the right (as you enter the lab) runs the Motive software used to setup the MoCap
- At the far left end of this desk there's a bunch of ethernet cables going down to a couple switches and a router. 
	- The two switches with lots of plugged in ethernet cables are the ones dedicated to the MoCap cameras. 
	- The cameras use PoE for power and comms.
	- Plug in the power to these switches to turn the cameras on -- usually people unplug the switches to turn the system off.
		- The power cables will likely be hanging together to the left side of the switches.
	- It takes the switches and cameras about a minute to fully start up.
		- The Motive software will detect them automatically once they are fully operation.

## Setup For Use
- In Motive, load the project "crazyflie-WLOG" (check this name) in the default folder (also we should back this up to this repo).
	- This should automatically load the calibration for the cameras, and in the 3D viewer you should see them arranged around the workspace as they are in the lab.
		- If the cameras appear to all be in a straight line (their default assumed position before calibration), you'll need to load a calibration file. 
		- Go to "Open Project" and then load a calibration file. There are a few calibration files available in the same default folder where the project file sits. I've been using the one dated to October 2017.
	- This project will have the quadcopter and ping pong ball already registered as objects.
- As per Peter's instructions, go to View --> Data Streaming and under Advanced enter the IP of the receiving PC running ROS. This is the local network IP of the PC (or the VM on a PC) connected to the lab's wifi (name?)